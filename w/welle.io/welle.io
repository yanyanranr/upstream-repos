---
url:
- https://github.com/AlbrechtL/welle.io
pkgbuild_repo:
- aur-w/welle.io
- aur-w/welle.io-git
- aur-w/welle.io-soapysdr
- aur-w/welle.io-soapysdr-git

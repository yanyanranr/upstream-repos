# UPSTREAM REPOS
本仓库用于存放上游仓库地址，compass-ci服务会实时监控这个仓库中注册的所有上游仓库，当您的仓库有新的commit时，会自动触发测试任务，并且可在我们的网站中查看结果。

## 注册上游仓库
1. fork本仓库，以便之后通过pull request的方式提交文件

2. 创建目录及注册文件

目录创建规则：第一层目录的名称以上游仓库名称首个字符命名，第二层目录的名称以上游仓库名称命名，第二层目录下的文件以上游仓库名称命名

以mongodb/mongo项目为例：
```
mkdir -p m/mongo
echo 'url: https://github.com/mongodb/mongo.git' >  m/mongo/mongo
```
3. 通过pull request的方式完成注册

## 自动触发测试任务

1. 采用Pull Request的方式将适配好的测试用例添加到repo信息文件所在目录下的 jobs/ 目录中，对于上面的例子则是 `m/mongo/jobs` 。

如果有只适用于自己项目的测试脚本及输出（指lkp-tests仓未包含，且不通用），请提交到类似目录 `m/mongo/tests` 和 `m/mongo/stats` 。

如果是通用性的测试用例，欢迎贡献到仓库 [lkp-tests/jobs](https://gitee.com/wu_fengguang/lkp-tests/tree/master/jobs)

如何适配测试用例到compass-ci请参考：[add-testcase.md](https://gitee.com/wu_fengguang/lkp-tests/blob/master/doc/add-testcase.md)

2. 将所有的接口文件通过Pull Request的方式迁移到compass-ci后，在本仓库中对应目录修改或新增DEFAULTS文件，以添加测试命令参数。

DEFAULTS文件在upstream-repos仓库中存在多个，同一条路径上的DEFAULTS文件会依次加载。若有相同的key，内层的DEFAULTS文件内容会覆盖外层的。里面主要存着下级目录中的repos的通用的信息。

修改和新增DEFAULTS文件都应该在尽量下级的目录，以免对其他repo的影响。文件的典型样例如下
```
submit:
- command: testbox=vm-2p16g os=openeuler os_version=20.03 os_mount=cifs os_arch=aarch64 api-avx2neon.yaml
  branches:
  - master
  - next
- command: testbox=vm-2p16g os=openeuler os_version=20.03 os_mount=cifs os_arch=aarch64 other-avx2neon.yaml
  branches:
  - branch_name_a
  - branch_name_b

```
这里的branches为命令会应用的分支。branches项可以没有，若没有，则该条命令会应用到所有分支。

master/next这些是分支的名称，可以通过git show-ref --heads命令查询仓库的分支，并去掉前缀 `refs/heads/` 。这样的分支名称才能正确匹配上，并不能随意写。
